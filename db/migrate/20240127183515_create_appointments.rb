class CreateAppointments < ActiveRecord::Migration[7.1]
  def change
    create_table :appointments do |t|
      t.string :name
      t.date :date
      t.time :time
      t.string :patient
      t.string :type_of_appointment

      t.timestamps
    end
  end
end
