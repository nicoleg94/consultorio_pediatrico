class CreatePacients < ActiveRecord::Migration[7.1]
  def change
    create_table :pacients do |t|
      t.integer :id_patient
      t.string :name
      t.string :legal_representative_name
      t.string :email_1
      t.string :email_2
      t.integer :phone

      t.timestamps
    end
  end
end
